<?php
namespace Devg\Customertype\Observer;
use Magento\Customer\Api\CustomerRepositoryInterface;

class Registersuccess implements \Magento\Framework\Event\ObserverInterface
{
	/** @var CustomerRepositoryInterface */
    protected $customerRepository;
	
  public function execute(\Magento\Framework\Event\Observer $observer)
  {
       $accountController = $observer->getAccountController();
       $customer = $observer->getCustomer();
       $request = $accountController->getRequest();
       $customer_group = $request->getParam('user-type');
       if($customer_group === 'tutor'){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerObj = $objectManager->create('Magento\Customer\Model\Customer')
            ->load($customer->getId());
            $customerObj->setGroupId(2);
            $customerObj->save();
        }
     return $this;
  }
}