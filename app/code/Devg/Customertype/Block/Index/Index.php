<?php

namespace Devg\Customertype\Block\Index;
use Magento\Customer\Model\Session;

class Index extends \Magento\Framework\View\Element\Template {
/**
     * @var Session
     */
    protected $session;
    
    public function __construct(\Magento\Catalog\Block\Product\Context $context, Session $customerSession, array $data = []) {

        $this->session = $customerSession;
        $this->_isScopePrivate = true;
        parent::__construct($context, $data);

    }


    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function isLogedIn(){
        $customerId = $this->session->getCustomerId();
        if($customerId){
            return 1;
        }
        else{
            return 0;
        }
    }
}