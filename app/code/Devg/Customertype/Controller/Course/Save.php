<?php

namespace Devg\Customertype\Controller\Course;
 use Magento\Catalog\Model\Product;
 use Magento\Customer\Model\Session;
 
class Save extends \Magento\Framework\App\Action\Action
{
    protected $_product;
    protected $_session;
	    
    public function __construct(
	\Magento\Framework\App\Action\Context $context, 
	Product $productModel, 
	Session $session
	){
		parent::__construct($context);
        $this->_product = $productModel;
        $this->_session = $session;
        
    }
    public function execute()
    {
		$data = $this->getRequest()->getParams();
		$sku = $data['instructor_id'].'-'.$data['sku'];
		try{		
			$this->_product->setSku($sku); // Set your sku here
			$this->_product->setName($data['name']); // Name of Product 
			$this->_product->setShortDescription($data['short_description']);
			$this->_product->setDescription($data['description']);
			$category_id= array(4);
				// add your catagory id
			 $this->_product->setCategoryIds($category_id); 
			$this->_product->setWebsiteIds(array(1));
			$this->_product->setAttributeSetId(4); // Attribute set id
			$this->_product->setStatus(0); // Status on product enabled/ disabled 1/0
			$this->_product->setWeight(1); // weight of product
			$this->_product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
			$this->_product->setTaxClassId(0); // Tax class id
			$this->_product->setTypeId('downloadable'); // type of product (simple/virtual/downloadable/configurable)
			$this->_product->setPrice($data['price']); // price of product
			$this->_product->setStockData(
                        array(
                            'use_config_manage_stock' => 0,
                            'manage_stock' => 1,
                            'is_in_stock' => 1,
                            'qty' => 99999
                        )
                    );
					
					
			$this->_product->setInstructorId($data['instructor_id']);	
			$this->_product->setPrerequisites($data['prerequisites']);
			$this->_product->setLanguage($data['language']);
			$this->_product->setLevel($data['level']);
			$this->_product->setTargetStudent($data['target_student']);
			$this->_product->setWhatTheyLearn($data['what_they_learn']);
			$this->_product->setAboutTheInstructor($data['about_the_instructor']);

			
			$this->_product->save();
                        $this->messageManager->addSuccess(__('Your course successfully submited for review.'));
			// Adding Image to product
			// $imagePath = "sample.jpg"; // path of the image
			// $product->addImageToMediaGallery($imagePath, array('image', 'small_image', 'thumbnail'), false, false);
			// $product->save();
    } catch (\Exception $e) {
        $this->messageManager->addError($e->getMessage());
        return $resultRedirect->setPath('customertype/course/addcourse');
    }        
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('customer/account');
    }
}